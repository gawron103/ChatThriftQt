#ifndef MESSAGESSTORAGEHANDLER_H
#define MESSAGESSTORAGEHANDLER_H

#include "ChatServices_types.h"
#include "MessagesStorage.h"
#include "MessagesStoragReactor.h"

/**
 * @brief The MessagesStorageHandler class
 * Class which is a handler to MessagesStorage class.
 * Contains methods that can get all messages,
 * add new ones, register and unregister messages
 * storage reactors.
 */
class MessagesStorageHandler : public MessagesStorageIf
{
public:
    MessagesStorageHandler();

    /**
     * @brief addMessage -> Method which is responsible for adding new
     * message to the storage.
     * @param message -> Message from clients app.
     */
    void addMessage(const Message& message);

    /**
     * @brief getAllMessages -> Method which is responsible for
     * sending all messages storage to clients app.
     */
    void getAllMessages();

    /**
     * @brief registerMessagesStorageReactor -> Method which is
     * responsible fot registering new messages storage
     * reactor.
     * @param ip -> Ip address of client.
     * @param port -> Port on which reactor is running.
     * @param userID -> Unique id of clients app.
     */
    void registerMessagesStorageReactor(const std::string& ip, const int32_t port, const int32_t userID);

    /**
     * @brief unregisterMessagesStorageReactor -> Method which is responsible
     * for unregistering messages storage reactor.
     * @param userID -> Unique id of clients app.
     */
    void unregisterMessagesStorageReactor(const int32_t userID);

private:
    /**
     * @brief m_messages -> Vector of messages structs.
     */
    Messages m_messages;

    /**
     * @brief m_clients -> Map of messages storages reactors.
     */
    std::map<int, ::apache::thrift::stdcxx::shared_ptr<MessagesStoragReactorIf>> m_clients;
};

#endif // MESSAGESSTORAGEHANDLER_H
