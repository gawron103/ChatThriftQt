#include "messagesstoragehandler.h"

#include <iostream>

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>

//========================================================================================================================
//========================================================================================================================

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;

//========================================================================================================================
//========================================================================================================================

MessagesStorageHandler::MessagesStorageHandler()
{

}

//========================================================================================================================
//========================================================================================================================

void MessagesStorageHandler::addMessage(const Message &message)
{
    m_messages.push_back(message);

    std::cout << "NEW MESSAGE REGISTERED" << std::endl;
    std::cout << "======================" << std::endl;
    std::cout << "MESSAGE TEXT: " << message.text << std::endl;
    std::cout << "MESSAGE TIMESTAMP: " << message.time << std::endl;
    std::cout << "MESSAGE AUTHOR: " << message.nickname << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    for (auto client = m_clients.begin(); client != m_clients.end(); ++client)
    {
        client->second->updateChat(message);
    }
}

//========================================================================================================================
//========================================================================================================================

void MessagesStorageHandler::getAllMessages()
{
    for (uint32_t i{0}; i < m_messages.size(); ++i)
    {
        m_clients.rbegin()->second->updateChat(m_messages[i]);
    }
}

//========================================================================================================================
//========================================================================================================================

void MessagesStorageHandler::registerMessagesStorageReactor(const std::string &ip, const int32_t port, const int32_t userID)
{
    ::apache::thrift::stdcxx::shared_ptr<TTransport> socket(new TSocket(ip, port));
    ::apache::thrift::stdcxx::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
    ::apache::thrift::stdcxx::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

    ::apache::thrift::stdcxx::shared_ptr<MessagesStoragReactorClient> client(new MessagesStoragReactorClient(protocol));
    transport->open();

    m_clients.insert(std::make_pair(userID, client));

    std::cout << "CHAT REACTOR CONNECTED" << std::endl;
    std::cout << "======================" << std::endl;
    std::cout << "CURRENT CHAT REACTORS COUNT:\t" << m_clients.size() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
}

//========================================================================================================================
//========================================================================================================================

void MessagesStorageHandler::unregisterMessagesStorageReactor(const int32_t userID)
{
    for (auto client = m_clients.begin(); client != m_clients.end(); ++client)
    {
        if (client->first == userID)
        {
            m_clients.erase(client);
        }
    }

    std::cout << "CHAT REACTOR ERASED" << std::endl;
    std::cout << "======================" << std::endl;
    std::cout << "CURRENT CHAT REACTORS COUNT:\t" << m_clients.size() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
}

//========================================================================================================================
//========================================================================================================================
