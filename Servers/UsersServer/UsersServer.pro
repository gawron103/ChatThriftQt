TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    usersstoragehandler.cpp

HEADERS += \
    usersstoragehandler.h

#LAPTOP MACOSX
#---------------------------------------------------
#INCLUDEPATH += /usr/local/opt/thrift/include
#---------------------------------------------------

#LAPTOP UBUNTU
#---------------------------------------------------
# THRIFT HEADER FILES
#INCLUDEPATH += /usr/local/include/thrift

# THRIFT LIBS
#LIBS += -L/usr/local/lib -lthrift
#---------------------------------------------------

#IMAC MINT
#---------------------------------------------------
# THRIFT HEADER FILES
INCLUDEPATH += /usr/local/include/thrift

# THRIFT LIBS
LIBS += -L/usr/local/lib -lthrift
#---------------------------------------------------

#CHAT LIBS
LIBS +=-L/home/piotrek/Desktop/ChatThriftQt/Libraries -lChatLibrary

#CHAT INCLUDES
INCLUDEPATH += /home/piotrek/Desktop/ChatThriftQt/ChatLibrary
