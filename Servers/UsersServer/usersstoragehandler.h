#ifndef USERSSTORAGEHANDLER_H
#define USERSSTORAGEHANDLER_H

#include "ChatServices_types.h"
#include "UsersStorage.h"
#include "UsersStorageReactor.h"

class UsersStorageHandler : public UsersStorageIf
{
public:
    UsersStorageHandler();

    /**
     * @brief subscribeUser -> Method which is responsible for creating new User and
     *                         saving him into map.
     * @param username -> Nick that user choose.
     * @return -> user unique ID.
     */
    int32_t subscribeUser(const std::string& username);

    /**
     * @brief unsubscribeUser -> Method which is responsible for erasing
     *                           disconnected user from map.
     * @param userID -> ID of user that has been disconnected.
     */
    void unsubscribeUser(const int32_t userID);

    /**
     * @brief getAllUsers -> Method which is responsible for returning users count.
     */
    void getAllUsers();

    /**
     * @brief registerUsersStorageReactor -> Method which is responsible for creating
     *                                       new client reactor and saving him into map.
     * @param ip -> Clients ip.
     * @param port -> Clients port.
     * @param userID -> Clients unique ID.
     */
    void registerUsersStorageReactor(const std::string& ip, const int32_t port, const int32_t userID);

    /**
     * @brief unregisterUsersReactor -> Method which is responsible fot erasing disconnected
     *                                  client from map.
     * @param userID -> ID of client that has been disconnected.
     */
    void unregisterUsersReactor(const int32_t userID);

private:
    /**
     * @brief users -> Map that holds connected users.
     */
    Users users;

    std::map<int32_t, ::apache::thrift::stdcxx::shared_ptr<UsersStorageReactorIf>> m_clients;
};

#endif // USERSSTORAGEHANDLER_H
