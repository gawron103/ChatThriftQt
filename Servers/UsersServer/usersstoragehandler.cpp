#include "usersstoragehandler.h"

#include <iostream>
#include <time.h>
#include <cassert>

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>

//========================================================================================================================
//========================================================================================================================

static uint32_t idGenerator{0};

//========================================================================================================================
//========================================================================================================================

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;

//========================================================================================================================
//========================================================================================================================

UsersStorageHandler::UsersStorageHandler()
{
    srand(time(NULL));
}

//========================================================================================================================
//========================================================================================================================

int32_t UsersStorageHandler::subscribeUser(const std::string &username)
{
    ++idGenerator;

    User user;
    user.userId = idGenerator;
    user.username = username;
    user.colorR = (rand() % 255) + 0;
    user.colorG = (rand() % 255) + 0;
    user.colorB = (rand() % 255) + 0;
    user.colorA = 255;

    users.insert(std::make_pair(idGenerator, user));

    std::cout << "NEW USER CONNECTED" << std::endl;
    std::cout << "==================" << std::endl;
    std::cout << "Username:\t" << user.username << std::endl;
    std::cout << "User ID:\t" << user.userId << std::endl;
    std::cout << "User R:\t" << user.colorR << std::endl;
    std::cout << "User G:\t" << user.colorG << std::endl;
    std::cout << "User B:\t" << user.colorB << std::endl;
    std::cout << "User A:\t" << user.colorA << std::endl;
    std::cout << "==================" << std::endl;
    std::cout << "CURRENT USERS COUNT:\t" << users.size() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    for (auto client = m_clients.begin(); client != m_clients.end(); ++client)
    {
        client->second->getConnectedUsers(user);
    }

    return idGenerator;
}

//========================================================================================================================
//========================================================================================================================

void UsersStorageHandler::unsubscribeUser(const int32_t userID)
{
    auto index = users.find(userID);

    assert(index != users.end());

    users.erase(index);

    std::cout << "USER DISCONNECTED" << std::endl;
    std::cout << "=================" << std::endl;
    std::cout << "USER WITH ID " << userID << " ERASED" << std::endl;
    std::cout << "USERS COUNT:\t" << users.size() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;

    for (auto client = m_clients.begin(); client != m_clients.end(); ++client)
    {
        client->second->deleteUser(userID);
    }
}

//========================================================================================================================
//========================================================================================================================

void UsersStorageHandler::getAllUsers()
{
    for (auto user = users.begin(); user != users.end(); ++user)
    {
        m_clients.rbegin()->second->getConnectedUsers(user->second);
    }
}

//========================================================================================================================
//========================================================================================================================

void UsersStorageHandler::registerUsersStorageReactor(const std::string &ip, const int32_t port, const int32_t userID)
{
    ::apache::thrift::stdcxx::shared_ptr<TTransport> socket(new TSocket(ip, port));
    ::apache::thrift::stdcxx::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
    ::apache::thrift::stdcxx::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

    ::apache::thrift::stdcxx::shared_ptr<UsersStorageReactorClient> client(new UsersStorageReactorClient(protocol));
    transport->open();

    m_clients.insert(std::make_pair(userID, client));

    std::cout << "USER REACTOR CONNECTED" << std::endl;
    std::cout << "======================" << std::endl;
    std::cout << "CURRENT USERS REACTORS COUNT:\t" << m_clients.size() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
}

//========================================================================================================================
//========================================================================================================================

void UsersStorageHandler::unregisterUsersReactor(const int32_t userID)
{
    for (auto client = m_clients.begin(); client != m_clients.end(); ++client)
    {
        if (client->first == userID)
        {
            m_clients.erase(client);
        }
    }

    std::cout << "USER REACTOR ERASED" << std::endl;
    std::cout << "======================" << std::endl;
    std::cout << "CURRENT USERS REACTORS COUNT:\t" << m_clients.size() << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
}

//========================================================================================================================
//========================================================================================================================
