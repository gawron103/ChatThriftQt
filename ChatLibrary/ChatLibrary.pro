#-------------------------------------------------
#
# Project created by QtCreator 2017-12-12T14:54:17
#
#-------------------------------------------------

QT       -= core gui

TARGET = ChatLibrary
TEMPLATE = lib

DEFINES += CHATLIBRARY_LIBRARY

INCLUDEPATH += /usr/local/include/thrift/

LIBS += -L/usr/local/lib -lthrift

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        chatlibrary.cpp \
    ChatServices_constants.cpp \
    ChatServices_types.cpp \
    MessagesStorage.cpp \
    MessagesStoragReactor.cpp \
    UsersStorage.cpp \
    UsersStorageReactor.cpp

HEADERS += \
        chatlibrary.h \
    ChatServices_constants.h \
    ChatServices_types.h \
    MessagesStorage.h \
    MessagesStoragReactor.h \
    UsersStorage.h \
    UsersStorageReactor.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    ChatServices.thrift
