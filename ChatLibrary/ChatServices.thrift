//===========================================================================================
//===========================================================================================

/**
 * User -> Struct that represents user (client app).
 **/
struct User
{
    1:i32 userId,
    2:string username,
    3:i32 colorR;
    4:i32 colorG,
    5:i32 colorB,
    6:i32 colorA,
}

//===========================================================================================
//===========================================================================================

/**
 * Message -> Struct that represents message sent by user.
 **/
struct Message
{
    1:string text,
    2:string time,
    3:string nickname
}

//===========================================================================================
//===========================================================================================

/**
 * Users -> Container for User instances.
 **/
typedef map<i32, User> Users

//===========================================================================================
//===========================================================================================

/**
 * Messages -> Container for Messages interfaces.
 **/
typedef list<Message> Messages

//===========================================================================================
//===========================================================================================

/**
 * ConnectedUsers -> Container for User instances.
 **/
typedef list<User> ConnectedUsers

//===========================================================================================
//===========================================================================================

/**
 * UsersStorage -> Service for handling clients connecting
 *                 to the users server. It could add new client,
 *                 remove him, register that client reactor and
 *                 unregister him.
 **/
service UsersStorage
{
    i32 subscribeUser(1:string username),
    void unsubscribeUser(1:i32 userID),
    void getAllUsers(),
    void registerUsersStorageReactor(1:string ip, 2:i32 port, 3:i32 userID),
    void unregisterUsersReactor(1:i32 userID)
}

//===========================================================================================
//===========================================================================================

/**
 * UsersStorageReactor -> Service for handling clients server from client side.
 *                        It could update users and delete them.
 **/
service UsersStorageReactor
{
    void getConnectedUsers(1:User user),
    void deleteUser(1:i32 userID)
}

//===========================================================================================
//===========================================================================================

/**
 * MessagesStorage -> Service for handling messages between users.
 *                    It could add message, get all of them, register
 *                    messages reactor and unregister them.
 **/
service MessagesStorage
{
    void addMessage(1:Message message),
    void getAllMessages(),
    void registerMessagesStorageReactor(1:string ip, 2:i32 port, 3:i32 userID),
    void unregisterMessagesStorageReactor(1:i32 userID),
}

//===========================================================================================
//===========================================================================================

/**
 * MessagesStoragReactor -> Service for handling messages server from client side.
 *                          It could update client current chat.
 **/
service MessagesStoragReactor
{
    void updateChat(1:Message message)
}

//===========================================================================================
//===========================================================================================
