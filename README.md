## ChatThriftQt


Last Modified: 2018-01-14

## Introduction

ChatThriftQt has been created for the need to pass the subject "Languages and
programming paradigms". It has been written in C++ with usage of Qt framework and Apache Thrift.

It is a chat between machines working by RPC (Remote Procedure Call) protocol.

The application has been made for Linux, due to the libraries contained in the repository. However, both Qt and Thrift are multi-platform technologies, so after compiling new libraries, the application will run on Windows or macOS.

## Project Hierarchy

    ChatThriftQt/

        ChatLibrary/
        
            Contains thrift services generated for C++ language.
        
        Libraries/
    
            Contains compiled, ready to use libraries (linux).
    
        Servers/
        
            Contains server for connected clients and for messages that clients sent.
        
            ChatServer/
        
            UsersServer/
        
        Client/
        
            Contains simple client application with GUI.
        
## Requirements

You must have the following things installed:

**`Qt5 (It would be good to have Qt Creator IDE too)`**
        
**`Boost 1.53 or later`**
        
**`Apache Thrift`**
        
## Installation

First of all install Qt software if you have not done it yet. Remember, Qt is awesome :) !
Installation process ---> [Qt installation](http://doc.qt.io/qt-5/gettingstarted.html)

Also you should definitelly install Apache Thrift.
Installation process and requirements ---> [Apache Thrift](https://github.com/apache/thrift)


If you are using Qt Creator just load .pro files for both servers and client. 
Also remember to modify INCLUDEPATHs and LIBS in pro files, so that they suit yout paths.

## Usage

Remember that you need to have both servers launched before trying to connect!

In client app input your nickname, ip address of machine on which servers are running, port
and click connect. If everything is ok you should see four pop up windows. Also in
the upper part of app you should see a square with letter (that's your avatar).

From now on simply write text and click send.
