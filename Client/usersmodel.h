#ifndef USERSMODEL_H
#define USERSMODEL_H

#include <QAbstractListModel>
#include <QIcon>

/**
 * @brief The UsersModel class
 * Model of users connected to the users server.
 * It can add, remove and display users.
 */
class UsersModel : public QAbstractListModel
{
public:
    UsersModel();

    /**
     * @brief rowCount -> Method that return amount of users in storage.
     * @param parent
     * @return -> Amount of users.
     */
    int rowCount(const QModelIndex &parent) const;

    /**
     * @brief data -> Method that displays users in view.
     * @param index -> Index of user in QMap.
     * @param role -> Role of view.
     * @return -> QVariant.
     */
    QVariant data(const QModelIndex &index, int role) const;

    /**
     * @brief add -> Method that adds new user to storage.
     * @param id -> Id of user and also key on which it is kept
     * in storage.
     * @param avatar -> Avatar created for specific user.
     */
    void add(const int &id, const QIcon &avatar);

    /**
     * @brief removeRows -> Method that removes specific user from storage.
     * @param row -> Number of user in storage.
     * @param count -> Amount of users in storage.
     * @param parent
     * @return -> True if user has been deleted or false if not.
     */
    bool removeRows(int row, int count, const QModelIndex &parent);

private:
    /**
     * @brief m_connectedUsers -> Storage of users from server.
     */
    QMap<int, QIcon> m_connectedUsers;
};

#endif // USERSMODEL_H
