#ifndef CHATTHREAD_H
#define CHATTHREAD_H

#include <QThread>

#include <memory>

#include "chatmodel.h"

/**
 * @brief The ChatThread class
 * Class responsible for creating new thread for
 * MessagesStorageReactor. There are server in thread, which
 * is listening for connections from MessagesServer.
 */
class ChatThread : public QThread
{
public:
    ChatThread(const uint32_t &id, const std::shared_ptr<ChatModel> model);

protected:
    /**
     * @brief run -> Method that runs server.
     */
    void run();

private:
    /**
     * @brief m_clientsID -> Unique ID of client.
     */
    uint32_t m_clientsID;

    /**
     * @brief m_chatModel -> Pointer to instance of chat model.
     */
    std::shared_ptr<ChatModel> m_chatModel = nullptr;
};

#endif // CHATTHREAD_H
