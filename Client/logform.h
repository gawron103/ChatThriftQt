#ifndef LOGFORM_H
#define LOGFORM_H

#include <QWidget>

namespace Ui {
class LogForm;
}

/**
 * @brief The LogForm class
 * Class responsible for login to the servers.
 */
class LogForm : public QWidget
{
    Q_OBJECT

public:
    explicit LogForm(QWidget *parent = 0);
    ~LogForm();

private:
    Ui::LogForm *ui;

    /**
     * @brief exitApp -> Method that emits signal for
     * closing app.
     */
    void exitApp();

    /**
     * @brief connectToServers -> Method responsible for
     * connecting to the user and chat servers.
     */
    void connectToServers();

    /**
     * @brief checkDataToConnect -> Method that checks if
     * user filled out all login fields.
     * @return -> Return true if all firlds was filled out.
     */
    bool checkDataToConnect();

signals:
    /**
     * @brief closeApp -> Signal for close app.
     */
    void closeApp();

    /**
     * @brief connection -> Signal for connecting to the user
     * server.
     * @param ip -> User server ip.
     * @param port -> User server port.
     * @param nickname -> Users nickname.
     */
    void connection(const std::string &ip, const uint32_t &port, const std::string &nickname);

    /**
     * @brief sendNickname -> Signal for returning user
     * nickname to MainWindow class.
     * @param nickname -> User nickname.
     */
    void sendNickname(const std::string &nickname);
};

#endif // LOGFORM_H
