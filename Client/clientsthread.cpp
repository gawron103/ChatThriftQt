#include "clientsthread.h"
#include "usersstoragereactorhandler.h"

#include <QDebug>

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

//========================================================================================================================
//========================================================================================================================

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

//========================================================================================================================
//========================================================================================================================

ClientsThread::ClientsThread(const uint32_t &id, const std::shared_ptr<UsersModel> model) :
    m_clientsID(id),
    m_usersModel(model)
{

}

//========================================================================================================================
//========================================================================================================================

void ClientsThread::run()
{
    const uint32_t PORT{6060 + m_clientsID};
    ::apache::thrift::stdcxx::shared_ptr<UsersStorageReactorHandler> handler(new UsersStorageReactorHandler(m_usersModel));
    ::apache::thrift::stdcxx::shared_ptr<TProcessor> processor(new UsersStorageReactorProcessor(handler));
    ::apache::thrift::stdcxx::shared_ptr<TServerTransport> serverTransport(new TServerSocket(PORT));
    ::apache::thrift::stdcxx::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
    ::apache::thrift::stdcxx::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

    TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);

    qDebug() << "Users Reactor Server started at port:\t" << PORT;

    server.serve();
}

//========================================================================================================================
//========================================================================================================================
