#include "chatmodel.h"

//========================================================================================================================
//========================================================================================================================

ChatModel::ChatModel()
{

}

//========================================================================================================================
//========================================================================================================================

int ChatModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return m_messages.size();
}

//========================================================================================================================
//========================================================================================================================

QVariant ChatModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() >= m_messages.size() ||
            role != Qt::DisplayRole)
    {
        return QVariant();
    }

    constexpr uint32_t MAX_CHARS_PER_LINE{80};
    const uint32_t CHARS_COUNT{static_cast<uint32_t>(m_messages[index.row()].text.length())};

    QString readyMsg = QString::fromStdString(m_messages[index.row()].nickname) +
            " at " + QString::fromStdString(m_messages[index.row()].time) + " says:\n";

    for (uint32_t i{0}; i < CHARS_COUNT; ++i)
    {
        readyMsg += QString::fromStdString(m_messages[index.row()].text)[i];

        if (0 == i % MAX_CHARS_PER_LINE && i > 0)
        {
            readyMsg += "\n";
        }
    }

    readyMsg += "\n";

    return readyMsg;
}

//========================================================================================================================
//========================================================================================================================

void ChatModel::add(const Message &message)
{
    beginInsertRows(QModelIndex(), m_messages.size(), m_messages.size());
    m_messages.append(message);
    endInsertRows();
}

//========================================================================================================================
//========================================================================================================================
