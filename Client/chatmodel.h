#ifndef CHATMODEL_H
#define CHATMODEL_H

#include <QAbstractListModel>

#include <memory>

#include "ChatServices_types.h"

/**
 * @brief The ChatModel class
 * Class responsible for storing, adding and displaying messages from server.
 */
class ChatModel : public QAbstractListModel
{
    Q_OBJECT
public:
    ChatModel();

    /**
     * @brief rowCount -> Method that return current amount of messages.
     * @param parent
     * @return -> Amount of messages.
     */
    int rowCount(const QModelIndex &parent) const;

    /**
     * @brief data -> Method that displays messages in view.
     * @param index -> Index of message in QVector.
     * @param role -> Role of view.
     * @return -> QVariant;
     */
    QVariant data(const QModelIndex &index, int role) const;

    /**
     * @brief add -> Method that adds new message to storage.
     * @param message -> Message from server.
     */
    void add(const Message &message);

private:
    /**
     * @brief m_messages -> Storage of messagess from server.
     */
    QVector<Message> m_messages;
};

#endif // CHATMODEL_H
