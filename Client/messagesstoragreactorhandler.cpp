#include "messagesstoragreactorhandler.h"

#include <QDebug>

//========================================================================================================================
//========================================================================================================================

MessagesStoragReactorHandler::MessagesStoragReactorHandler(const std::shared_ptr<ChatModel> model) :
    m_model(model)
{

}

//========================================================================================================================
//========================================================================================================================

void MessagesStoragReactorHandler::updateChat(const Message &message)
{
    m_model.get()->add(message);
}

//========================================================================================================================
//========================================================================================================================
