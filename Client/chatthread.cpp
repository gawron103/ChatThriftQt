#include "chatthread.h"

#include <QDebug>

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

#include "messagesstoragreactorhandler.h"

//========================================================================================================================
//========================================================================================================================

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

//========================================================================================================================
//========================================================================================================================

ChatThread::ChatThread(const uint32_t &id, const std::shared_ptr<ChatModel> model) :
    m_clientsID(id),
    m_chatModel(model)
{

}

//========================================================================================================================
//========================================================================================================================

void ChatThread::run()
{
    const uint32_t PORT{7070 + m_clientsID};

    ::apache::thrift::stdcxx::shared_ptr<MessagesStoragReactorHandler> handler(new MessagesStoragReactorHandler(m_chatModel));
    ::apache::thrift::stdcxx::shared_ptr<TProcessor> processor(new MessagesStoragReactorProcessor(handler));
    ::apache::thrift::stdcxx::shared_ptr<TServerTransport> serverTransport(new TServerSocket(PORT));
    ::apache::thrift::stdcxx::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
    ::apache::thrift::stdcxx::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

    TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);

    qDebug() << "Chat Reactor Server started at port:\t" << PORT;

    server.serve();
}

//========================================================================================================================
//========================================================================================================================
