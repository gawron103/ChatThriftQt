#ifndef IPGETTER_H
#define IPGETTER_H

#include <string>

/**
 * @brief The IpGetter class
 * Class responsible for getting ip address
 * of machine.
 */
class IpGetter
{
public:
    IpGetter();

    /**
     * @brief getIpAddress -> Method that checks for ip address.
     * @return -> Ip address of machine.
     */
    std::string getIpAddress();
};

#endif // IPGETTER_H
