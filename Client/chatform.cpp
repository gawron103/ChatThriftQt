#include "chatform.h"
#include "ui_chatform.h"

#include <QDateTime>
#include <QDebug>
#include <QMessageBox>

//========================================================================================================================
//========================================================================================================================

ChatForm::ChatForm(const std::string nickname, const std::shared_ptr<UsersModel> model, const std::shared_ptr<ChatModel> chatModel, std::shared_ptr<MessagesStorageClient> client, QWidget *parent) :
    ui(new Ui::ChatForm),
    M_USERNAME(nickname),
    m_userModel(model),
    m_chatModel(chatModel),
    m_messagesStorageClient(client),
    QWidget(parent)
{
    ui->setupUi(this);

    ui->lw_clients->setAutoFillBackground(true);
    ui->lw_clients->setFocusPolicy(Qt::NoFocus);
    ui->lw_clients->setSelectionMode(QAbstractItemView::NoSelection);
    ui->lw_clients->setSpacing(1);
    ui->lw_clients->setStyleSheet("background-color: transparent;");
    ui->lw_clients->setModel(m_userModel.get());

    ui->lw_messages->setModel(m_chatModel.get());
    ui->lw_messages->setAutoFillBackground(true);

    connect(ui->le_messageInput, &QLineEdit::returnPressed, this, &ChatForm::sendMessage);
    connect(ui->pb_send, &QPushButton::clicked, this, &ChatForm::sendMessage);
}

//========================================================================================================================
//========================================================================================================================

ChatForm::~ChatForm()
{
    delete ui;
}

//========================================================================================================================
//========================================================================================================================

void ChatForm::sendMessage()
{
    if (!isMessageValid())
    {
        return;
    }

    Message message;
    message.text = ui->le_messageInput->text().toStdString();
    message.time = QDateTime::currentDateTime().toString("hh:mm").toStdString();
    message.nickname = M_USERNAME;

    try
    {
        m_messagesStorageClient.get()->addMessage(message);
    }
    catch (const std::exception &e)
    {
        qDebug() << e.what();

        QMessageBox::critical(this, "Lost connection", "Connection with servers "
                                                       "has been lost. "
                                                       "Please reset application "
                                                       "and try again.");
    }

    ui->le_messageInput->clear();
}

//========================================================================================================================
//========================================================================================================================

bool ChatForm::isMessageValid()
{
    if (ui->le_messageInput->text().isEmpty())
    {
        return false;
    }

    return true;
}

//========================================================================================================================
//========================================================================================================================
