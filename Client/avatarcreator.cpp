#include "avatarcreator.h"

#include <QPainter>
#include <QPixmap>

//========================================================================================================================
//========================================================================================================================

AvatarCreator::AvatarCreator()
{

}

//========================================================================================================================
//========================================================================================================================

QIcon AvatarCreator::drawAvatar(const User &user)
{
    constexpr uint32_t AVATAR_SIDE{50};
    constexpr uint32_t LETTER_SIZE{40};

    const QChar userCharacter = getCharacter(QString::fromStdString(user.username));

    QColor color(user.colorR, user.colorG, user.colorB, user.colorA);

    QPixmap pixmap(AVATAR_SIDE, AVATAR_SIDE);
    pixmap.fill(color);

    QFont font;
    font.setPixelSize(LETTER_SIZE);

    QPainter painter(&pixmap);
    painter.setFont(font);
    painter.setPen(QColor("white"));

    pixmap.fill(color);

    painter.drawText(pixmap.rect(), Qt::AlignCenter, userCharacter);

    QIcon avatar(pixmap);

    return avatar;
}

//========================================================================================================================
//========================================================================================================================

QChar AvatarCreator::getCharacter(const QString &nick) const
{
    return nick[0];
}
