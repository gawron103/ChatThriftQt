#include "usersmodel.h"

#include <QDebug>

//========================================================================================================================
//========================================================================================================================

UsersModel::UsersModel()
{

}

//========================================================================================================================
//========================================================================================================================

int UsersModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return m_connectedUsers.size();
}

//========================================================================================================================
//========================================================================================================================

QVariant UsersModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() >= m_connectedUsers.size() || role != Qt::DecorationRole)
    {
        return QVariant();
    }

    return m_connectedUsers.values().at(index.row());
}

//========================================================================================================================
//========================================================================================================================

void UsersModel::add(const int &id, const QIcon &avatar)
{
    beginInsertRows(QModelIndex(), m_connectedUsers.size(), m_connectedUsers.size());
    m_connectedUsers.insert(id, avatar);
    endInsertRows();
}

//========================================================================================================================
//========================================================================================================================

bool UsersModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);

    const uint32_t PREVIOUS_AMOUNT = m_connectedUsers.count();
    const uint32_t LAST = row + count + 1;

    beginRemoveRows(QModelIndex(), row, LAST);
    m_connectedUsers.remove(row);
    endRemoveRows();

    if (static_cast<uint32_t>(m_connectedUsers.size()) == PREVIOUS_AMOUNT + 1)
    {
        return true;
    }

    return false;
}

//========================================================================================================================
//========================================================================================================================
