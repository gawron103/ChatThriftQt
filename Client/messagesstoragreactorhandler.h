#ifndef MESSAGESSTORAGREACTORHANDLER_H
#define MESSAGESSTORAGREACTORHANDLER_H

#include <memory>

#include "MessagesStoragReactor.h"

#include "chatmodel.h"

/**
 * @brief The MessagesStoragReactorHandler class
 * Class which is a handler to MessagesStorageReactor class.
 * Contains methods that MessagesServer can call.
 */
class MessagesStoragReactorHandler : virtual public MessagesStoragReactorIf
{
public:
    MessagesStoragReactorHandler(const std::shared_ptr<ChatModel> model);

    /**
     * @brief updateChat -> Method responsible for adding new messages
     * from server to the messages model.
     * @param message -> Message struct from server.
     */
    void updateChat(const Message &message);

private:
    /**
     * @brief m_model -> Pointer to instance of messages model.
     */
    std::shared_ptr<ChatModel> m_model = nullptr;
};

#endif // MESSAGESSTORAGREACTORHANDLER_H
