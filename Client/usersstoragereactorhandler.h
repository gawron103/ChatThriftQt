#ifndef USERSSTORAGEREACTORHANDLER_H
#define USERSSTORAGEREACTORHANDLER_H

#include <memory>

#include "UsersStorageReactor.h"

#include "usersmodel.h"

/**
 * @brief The UsersStorageReactorHandler class
 * Class hich is handler to UsersStorageReactor class.
 * Contains methods that UsersServer can call.
 */
class UsersStorageReactorHandler : virtual public UsersStorageReactorIf
{
public:
    UsersStorageReactorHandler(const std::shared_ptr<UsersModel> model);

    /**
     * @brief getConnectedUsers -> Method responsible for adding new Users
     * from server to the users model.
     * @param user -> User stryct from server.
     */
    void getConnectedUsers(const User& user);

    /**
     * @brief deleteUser -> Method responsible for erasing chosen user
     * from storage.
     * @param userID -> Unique ID of chosen user.
     */
    void deleteUser(const int32_t userID);

private:
    /**
     * @brief m_model -> Pointer to instance of users model.
     */
    std::shared_ptr<UsersModel> m_model = nullptr;
};

#endif // USERSSTORAGEREACTORHANDLER_H
