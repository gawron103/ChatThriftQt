#include "ipgetter.h"

#include <QList>
#include <QHostAddress>
#include <QNetworkInterface>
#include <QDebug>

//========================================================================================================================
//========================================================================================================================

IpGetter::IpGetter()
{

}

//========================================================================================================================
//========================================================================================================================

std::string IpGetter::getIpAddress()
{
    QList<QHostAddress> list = QNetworkInterface::allAddresses();

    std::string ip = "";

    for(int nIter=0; nIter<list.count(); nIter++)

    {
        if(!list[nIter].isLoopback())
            if (list[nIter].protocol() == QAbstractSocket::IPv4Protocol )
                qDebug() << list[nIter].toString();
                    ip = list[nIter].toString().toStdString();
    }

    return ip;
}

//========================================================================================================================
//========================================================================================================================
