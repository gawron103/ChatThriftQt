#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <memory>

#include "UsersStorage.h"
#include "MessagesStorage.h"

#include "logform.h"
#include "chatform.h"
#include "clientsthread.h"
#include "chatthread.h"
#include "usersmodel.h"
#include "chatmodel.h"

namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class
 * Class which is responsible for app main window.
 * Contains all views and informations needed to
 * communication between servers and app.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    /**
     * @brief m_clientID -> Unique id of client given by users server.
     */
    static uint32_t m_clientID;

    /**
     * @brief m_usersServerPort -> Users server port given by user.
     */
    static uint32_t m_usersServerPort;

    /**
     * @brief m_usersServerIP -> Users server ip address given by user.
     */
    static std::string m_usersServerIP;

    /**
     * @brief m_machineIpAddr -> Ip addres of machine on which
     * client application is running.
     */
    std::string m_machineIpAddr;

    /**
     * @brief m_logScreen -> Pointer to instance of log in view.
     */
    std::unique_ptr<LogForm> m_logScreen = nullptr;

    /**
     * @brief m_chatScreen -> Pointer to instance of chat view.
     */
    std::unique_ptr<ChatForm> m_chatScreen = nullptr;

    /**
     * @brief m_clientsThread -> Pointer to instance of users thread.
     */
    std::unique_ptr<ClientsThread> m_clientsThread = nullptr;

    /**
     * @brief m_chatThread -> Pointer to instance of messages thread.
     */
    std::unique_ptr<ChatThread> m_chatThread = nullptr;

    /**
     * @brief m_usersServerClient -> Pointer to instance of users server client.
     */
    std::unique_ptr<UsersStorageClient> m_usersServerClient = nullptr;

    /**
     * @brief m_messagesServerClient -> Pointer to instance of messages server client.
     */
    std::shared_ptr<MessagesStorageClient> m_messagesServerClient = nullptr;


    /**
     * @brief m_usersModel ->  -> Pointer to instance of users model.
     */
    std::shared_ptr<UsersModel> m_usersModel = nullptr;

    /**
     * @brief m_chatModel -> Pointer to instance of messages model.
     */
    std::shared_ptr<ChatModel> m_chatModel = nullptr;


    /**
     * @brief connectToServers -> Method which is responsible for connecting to
     * both servers.
     * @param ip -> Ip address of servers given by user.
     * @param port -> Port of users server given by user.
     * @param nick -> Nick which user choose.
     */
    void connectToServers(const std::string &ip, const uint32_t &port, const std::string &nick);

    /**
     * @brief connectToUsersServer -> Method which is responsible for connecting client to
     * users server.
     * @param nick -> Nick which user choose.
     */
    void connectToUsersServer(const std::string &nick);

    /**
     * @brief connectToChatServer -> Method which is responsible for connecting to
     * messages server.
     */
    void connectToChatServer();

    /**
     * @brief activateChatScreen -> Method which is responsible for switching between
     * log in and chat screen.
     * @param nick -> Nick which user choose.
     */
    void activateChatScreen(const std::string &nick);

    /**
     * @brief createAndRegisterUsersServerReactor -> Method which is responsible for
     * creating and registering users server reactor.
     */
    void createAndRegisterUsersServerReactor();

    /**
     * @brief createAndRegisterChatServerReactor -> Method which is responsible for
     * creating and registering messages server reactor.
     */
    void createAndRegisterChatServerReactor();

    /**
     * @brief changeWindowTitle -> Method which is responsible for changing title of
     * app main window.
     * @param nickname -> Nick which user choose.
     */
    void changeWindowTitle(const std::string &nickname);

    /**
     * @brief disconnectFromUsersServer -> Method which is responsible for disconnecting
     * from users server.
     */
    void disconnectFromUsersServer();
};

#endif // MAINWINDOW_H
