#-------------------------------------------------
#
# Project created by QtCreator 2017-05-02T14:37:46
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Client
TEMPLATE = app
CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#LAPTOP MACOSX
#---------------------------------------------------
#INCLUDEPATH += /usr/local/opt/thrift/include
#---------------------------------------------------

#LAPTOP UBUNTU
#---------------------------------------------------
# THRIFT HEADER FILES
#INCLUDEPATH += /usr/local/include/thrift

# THRIFT LIBS
#LIBS += -L/usr/local/lib -lthrift
#---------------------------------------------------

#IMAC MINT
#---------------------------------------------------
# THRIFT HEADER FILES
INCLUDEPATH += /usr/local/include/thrift

# THRIFT LIBS
LIBS += -L/usr/local/lib -lthrift
#---------------------------------------------------

#CHAT LIBS
LIBS +=-L/home/piotrek/Desktop/ChatThriftQt/Libraries -lChatLibrary

#CHAT INCLUDES
INCLUDEPATH += /home/piotrek/Desktop/ChatThriftQt/ChatLibrary

SOURCES += main.cpp \
	chatform.cpp \
	clientsthread.cpp \
	logform.cpp \
	mainwindow.cpp \
	usersstoragereactorhandler.cpp \
    usersmodel.cpp \
    avatarcreator.cpp \
    chatmodel.cpp \
    chatthread.cpp \
    messagesstoragreactorhandler.cpp \
    ipgetter.cpp

HEADERS  += chatform.h \
	clientsthread.h \
	logform.h \
	mainwindow.h \
	usersstoragereactorhandler.h \
    usersmodel.h \
    avatarcreator.h \
    chatmodel.h \
    chatthread.h \
    messagesstoragreactorhandler.h \
    ipgetter.h

FORMS    += chatform.ui \
	logform.ui \
	mainwindow.ui
