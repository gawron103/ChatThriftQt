#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QMessageBox>

#include <iostream>

#include "ipgetter.h"

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>

//========================================================================================================================
//========================================================================================================================

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;

//========================================================================================================================
//========================================================================================================================

uint32_t MainWindow::m_clientID{0};
uint32_t MainWindow::m_usersServerPort{0};
std::string  MainWindow::m_usersServerIP{"0.0.0.0"};

//========================================================================================================================
//========================================================================================================================

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_machineIpAddr("0.0.0.0")
{
    ui->setupUi(this);

    m_logScreen = std::make_unique<LogForm>();

    ui->sw_menu->addWidget(m_logScreen.get());
    ui->sw_menu->setCurrentWidget(m_logScreen.get());

    connect(m_logScreen.get(), &LogForm::closeApp, this, &MainWindow::close);
    connect(m_logScreen.get(), &LogForm::connection, this, &MainWindow::connectToServers);
    connect(m_logScreen.get(), &LogForm::sendNickname, this, &MainWindow::changeWindowTitle);
}

//========================================================================================================================
//========================================================================================================================

MainWindow::~MainWindow()
{
    disconnectFromUsersServer();

    m_messagesServerClient.get()->unregisterMessagesStorageReactor(m_clientID);

    try
    {
        m_clientsThread.get()->exit(0);
    }
    catch (const std::exception &e)
    {
        qDebug() << e.what();
    }

    try
    {
        m_chatThread.get()->exit(0);
    }
    catch (const std::exception &e)
    {
        qDebug() << e.what();
    }

    delete ui;
}

//========================================================================================================================
//========================================================================================================================

void MainWindow::connectToServers(const std::string &ip, const uint32_t &port, const std::string &nick)
{
    m_usersServerIP = ip;
    m_usersServerPort = port;

    try
    {
        connectToUsersServer(nick);
        connectToChatServer();
        activateChatScreen(nick);
    }
    catch (const std::exception &e)
    {
        qDebug() << e.what();
    }

    qDebug() << "ID FROM SERVER:\t" << m_clientID;
}

//========================================================================================================================
//========================================================================================================================

void MainWindow::connectToUsersServer(const std::string &nick)
{
    try
    {
        ::apache::thrift::stdcxx::shared_ptr<TTransport> socket(new TSocket(m_usersServerIP, m_usersServerPort));
        ::apache::thrift::stdcxx::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
        ::apache::thrift::stdcxx::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

        m_usersServerClient = std::make_unique<UsersStorageClient>(protocol);
        transport->open();

        m_clientID = m_usersServerClient.get()->subscribeUser(nick);

        QMessageBox::information(this, "Connected",
                                 "You are connected "
                                 "with users server");

        m_usersModel = std::make_shared<UsersModel>();

        IpGetter ip;
        m_machineIpAddr = ip.getIpAddress();

        createAndRegisterUsersServerReactor();

        m_usersServerClient.get()->getAllUsers();
    }
    catch (const std::exception &e)
    {
        qDebug() << e.what();
    }
}

//========================================================================================================================
//========================================================================================================================

void MainWindow::connectToChatServer()
{
    const std::string CHAT_SERVER_IP = m_usersServerIP;
    constexpr uint32_t CHAT_SERVER_PORT{8080};

    try
    {
        ::apache::thrift::stdcxx::shared_ptr<TTransport> socket(new TSocket(CHAT_SERVER_IP, CHAT_SERVER_PORT));
        ::apache::thrift::stdcxx::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
        ::apache::thrift::stdcxx::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

        m_messagesServerClient = std::make_shared<MessagesStorageClient>(protocol);
        transport->open();

        QMessageBox::information(this, "Connected",
                                 "You are connected "
                                 "with messages server");

        m_chatModel = std::make_shared<ChatModel>();

        createAndRegisterChatServerReactor();
    }
    catch (const std::exception &e)
    {
        qDebug() << e.what();
    }
}

//========================================================================================================================
//========================================================================================================================

void MainWindow::activateChatScreen(const std::string &nick)
{
    m_chatScreen = std::make_unique<ChatForm>(nick, m_usersModel, m_chatModel, m_messagesServerClient);

    ui->sw_menu->addWidget(m_chatScreen.get());
    ui->sw_menu->setCurrentWidget(m_chatScreen.get());
}

//========================================================================================================================
//========================================================================================================================

void MainWindow::createAndRegisterUsersServerReactor()
{
    try
    {
        const int REACTOR_PORT = 6060 + m_clientID;

        m_clientsThread = std::make_unique<ClientsThread>(m_clientID, m_usersModel);
        m_clientsThread.get()->start();

        QThread::msleep(100);

        m_usersServerClient.get()->registerUsersStorageReactor(m_machineIpAddr, REACTOR_PORT, m_clientID);
//        m_usersServerClient.get()->registerUsersStorageReactor(MACHINE_IP, REACTOR_PORT, m_clientID);

        QMessageBox::information(this, "Users reactor", "Users reactor registered");
    }
    catch(const std::exception &e)
    {
        QMessageBox::critical(this, "Users reactor", e.what());
    }
}

//========================================================================================================================
//========================================================================================================================

void MainWindow::createAndRegisterChatServerReactor()
{
    try
    {
        const int REACTOR_PORT = 7070 + m_clientID;

        m_chatThread = std::make_unique<ChatThread>(m_clientID, m_chatModel);
        m_chatThread.get()->start();

        QThread::msleep(100);

        m_messagesServerClient.get()->registerMessagesStorageReactor(m_machineIpAddr, REACTOR_PORT, m_clientID);
//        m_messagesServerClient.get()->registerMessagesStorageReactor(MACHINE_IP, REACTOR_PORT, m_clientID);

        m_messagesServerClient.get()->getAllMessages();

        QMessageBox::information(this, "Chat reactor", "Chat reactor registered");
    }
    catch (const std::exception &e)
    {
        QMessageBox::critical(this, "Chat reactor", e.what());
    }
}

//========================================================================================================================
//========================================================================================================================

void MainWindow::changeWindowTitle(const std::string &nickname)
{
    setWindowTitle("Chat - " + QString::fromStdString(nickname));
}

//========================================================================================================================
//========================================================================================================================

void MainWindow::disconnectFromUsersServer()
{
    try
    {
        ::apache::thrift::stdcxx::shared_ptr<TTransport> socket(new TSocket(m_usersServerIP, m_usersServerPort));
        ::apache::thrift::stdcxx::shared_ptr<TTransport> transport(new TBufferedTransport(socket));

        m_usersServerClient.get()->unsubscribeUser(m_clientID);
        transport->close();
    }
    catch (const std::exception &e)
    {
        qDebug() << e.what();
    }
}

//========================================================================================================================
//========================================================================================================================
