#ifndef AVATARCREATOR_H
#define AVATARCREATOR_H

#include <QString>
#include <QIcon>

#include "ChatServices_types.h"

/**
 * @brief The AvatarCreator class
 * Class responsible for creating users avatar.
 */
class AvatarCreator
{
public:
    AvatarCreator();

    /**
     * @brief drawAvatar -> Method that creates avatar.
     * @param user -> User struct containing nickname
     * and colors.
     * @return -> Created avatar.
     */
    QIcon drawAvatar(const User &user);

private:
    /**
     * @brief getCharacter -> method that cuts users
     * nickname.
     * @param nick -> User nickname.
     * @return -> First letter of users nickname.
     */
    QChar getCharacter(const QString &nick) const;
};

#endif // AVATARCREATOR_H
