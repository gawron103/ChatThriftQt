#ifndef CHATFORM_H
#define CHATFORM_H

#include <QWidget>

#include <memory>

#include "usersmodel.h"
#include "chatmodel.h"

#include "MessagesStorage.h"

namespace Ui {
class ChatForm;
}

/**
 * @brief The ChatForm class
 * Class that is view for chat. It contains view,
 * message input and send button.
 */
class ChatForm : public QWidget
{
    Q_OBJECT

public:
    explicit ChatForm(const std::string nickname, const std::shared_ptr<UsersModel> model,
                      const std::shared_ptr<ChatModel> chatModel,
                      std::shared_ptr<MessagesStorageClient> client, QWidget *parent = 0);
    ~ChatForm();

private:
    Ui::ChatForm *ui;

    /**
     * @brief M_USERNAME -> Username that user choosed.
     */
    const std::string M_USERNAME;

    /**
     * @brief m_userModel -> Pointer to instance of users model.
     */
    std::shared_ptr<UsersModel> m_userModel = nullptr;

    /**
     * @brief m_chatModel -> Pointer to instance of messages model.
     */
    std::shared_ptr<ChatModel> m_chatModel = nullptr;

    /**
     * @brief m_messagesStorageClient -> Pointer to instance of messages server client.
     */
    std::shared_ptr<MessagesStorageClient> m_messagesStorageClient= nullptr;

    /**
     * @brief sendMessage -> Method that sends message to the messages server.
     */
    void sendMessage();

    /**
     * @brief isMessageValid -> Method that checks if message is valid.
     * @return
     */
    bool isMessageValid();
};

#endif // CHATFORM_H
