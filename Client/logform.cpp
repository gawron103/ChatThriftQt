#include "logform.h"
#include "ui_logform.h"

#include <QMessageBox>

//========================================================================================================================
//========================================================================================================================

LogForm::LogForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LogForm)
{
    ui->setupUi(this);



    connect(ui->pb_exit, &QPushButton::clicked, this, &LogForm::exitApp);
    connect(ui->pb_connect, &QPushButton::clicked, this, &LogForm::connectToServers);
    connect(ui->le_ip, &QLineEdit::returnPressed, this, &LogForm::connectToServers);
    connect(ui->le_port, &QLineEdit::returnPressed, this, &LogForm::connectToServers);
    connect(ui->le_username, &QLineEdit::returnPressed, this, &LogForm::connectToServers);
}

//========================================================================================================================
//========================================================================================================================

LogForm::~LogForm()
{
    delete ui;
}

//========================================================================================================================
//========================================================================================================================

void LogForm::exitApp()
{
    emit closeApp();
}

//========================================================================================================================
//========================================================================================================================

void LogForm::connectToServers()
{
    if (!checkDataToConnect())
    {
        return;
    }

    std::string inputIP = (ui->le_ip->text()).toStdString();
    uint32_t inputPort = (ui->le_port->text()).toInt();
    std::string username = (ui->le_username->text()).toStdString();

    emit connection(inputIP, inputPort, username);
    emit sendNickname(username);
}

//========================================================================================================================
//========================================================================================================================

bool LogForm::checkDataToConnect()
{
    if (ui->le_ip->text().isEmpty() ||
        ui->le_port->text().isEmpty() ||
        ui->le_username->text().isEmpty())
    {
        QMessageBox::warning(this,
                             "CONNECTION ERROR",
                             "To connect fill IP, PORT"
                             " and USERNAME fields");
        return false;
    }

    return true;
}

//========================================================================================================================
//========================================================================================================================
