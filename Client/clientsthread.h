#ifndef CLIENTSTHREAD_H
#define CLIENTSTHREAD_H

#include <QThread>

#include <memory>

#include "usersmodel.h"

/**
 * @brief The ClientsThread class
 * Class responsible for creating new thread for
 * UsersStorageReactor. There are server in thread, which
 * is listening for connections from UsersServer.
 */
class ClientsThread : public QThread
{
public:
    ClientsThread(const uint32_t &id, const std::shared_ptr<UsersModel> model);

protected:
    /**
     * @brief run -> Method that runs server.
     */
    void run();

private:
    /**
     * @brief m_clientsID -> Unique ID of client.
     */
    uint32_t m_clientsID;

    /**
     * @brief m_usersModel -> Pointer to instance of users model.
     */
    std::shared_ptr<UsersModel> m_usersModel = nullptr;
};

#endif // CLIENTSTHREAD_H
